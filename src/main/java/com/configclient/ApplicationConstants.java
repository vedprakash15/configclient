package com.configclient;

public class ApplicationConstants {
    public static final String LOGIN = "/login";
    public static final String HOME = "/home";
    public static final String SIGNUP = "/registration";
}
