package com.configclient.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.configclient.ApplicationConstants.HOME;

@RestController
//TODO @RequestMapping("${application.version}")
@RequestMapping("/v1")
public class HomeController {

    //@Value("${test}")
    private String test;

    @GetMapping(HOME)
    public String sayHello() {
        System.out.println(test);
        return "Hello home";
    }
}
