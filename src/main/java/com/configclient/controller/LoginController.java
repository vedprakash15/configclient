package com.configclient.controller;

import com.configclient.model.Login;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.configclient.ApplicationConstants.LOGIN;

@RestController
//TODO @RequestMapping("${application.version}")
@RequestMapping("/v1")
public class LoginController {
    @PostMapping(LOGIN)
    public ResponseEntity<Object> login(@RequestBody Login login){

        return ResponseEntity.ok().body("Success");
    }
}
